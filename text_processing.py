# -*- coding: utf-8 -*-
import re
import codecs

d_tags = {
        'definition': 'ЗНАЧЕНИЕ',
        'synonyms': 'СИН:',
        'analogs': 'АНА:',
        'collocations': 'СОЧЕТАЕМОСТЬ',
        'constructions': 'КОНСТРУКЦИИ',
        'government': 'УПРАВЛЕНИЕ',
        'derivates': 'ДЕР:',
        'antonyms': 'АНТ:',
        'conversives': 'КОНВ:'
    }

# массив с ключевыми словами для комментариев
comment_key_words = []


# вспомогательная функия, убирает знак ≈ в начале строки
def cleaner(lines):
    for i in range(len(lines)):
        if lines[i].startswith('≈'):
            lines[i] = lines[i][1:]
    return lines

#Any Kopaeva

# отмечает автора словарной статьи, почти у каждой есть автор [А. А.]
def tagAuthor(lines):
    i = 0
    while i < len(lines):
        s = re.search(" (\[[А-ЯЁ]\. [А-ЯЁ]\.\])", lines[i])
        lines[i] = re.sub(" (\[[А-ЯЁ]\. [А-ЯЁ]\.\])", "", lines[i])
        if s:
            lines.insert(i + 1, "\t@author: " + s.groups()[0])
            i += 1
        i += 1

    return lines

#Any Kopaeva

# автор - последняя запись в словарной статье,
# можно добавить начало новой словарной статьи сразу после этого
# также стоит добавить какой-нибудь визуальный разделитель типа '-----'
def tagEntry(lines):
    lines = [""] + lines
    for i in range(1, len(lines)):
        if re.search("@author", lines[i - 1]):
            lines[i] = "@entry:" + lines[i]
    lines.pop(0)
    if lines[len(lines) - 1] == "@entry:":
        lines.pop()
    return lines

#Any Kopaeva

def cleanend(lines):
    i = len(lines) - 1
    while lines[i] == "":
        lines.pop()
        i -= 1
    if lines[i] == "@entry:":
        lines.pop()
    for i in range(len(lines)):
        if re.search("\[см. тж СОЧЕТАЕМОСТЬ\]", lines[i]):
            lines[i] = lines[i].replace("[см. тж СОЧЕТАЕМОСТЬ]", "")
    return lines

# add:
# entry split
# write func for I-numbered entries


#Any Kopaeva

# так как встречаются сл. статьи без автора
# нужно написать регулярное выражение, которое находит названия сл. статей
def resolveEntries(lines):
    letters = ("А", "Э", "И", "Ы", "О", "У", "Я", "Е", "Ё", "Ю")
    for i in range(len(lines)):
        if not((lines[i] != "") and (lines[i][0] == "@")):
            lines[i] = re.sub("^([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'\-]+)*[1-9]*),", "@entry:\\1", lines[i]) #для слов с пробелами ####
        if not((lines[i] != "") and (lines[i][0] == "@")):
            lines[i] = re.sub("^([А-ЯЁ\-]+'[А-ЯЁ\-]*)", "@entry:\\1", lines[i]) #для большей части слов
        if not((lines[i] != "") and (lines[i][0] == "@")):
            lines[i] = re.sub("^([А-ЯЁ\-]+[1-9]*, [А-ЯЁ\-]+)",  "@entry:\\1", lines[i]) #для слов типа АБОРДАЖ, только коротких
        if not((lines[i] != "") and (lines[i][0] == "@")):
            lines[i] = re.sub("^([А-ЯЁ\-]+ (и|или) [А-ЯЁ\-]+)",  "@entry:\\1", lines[i])
        #

    #Вставить разделители
    i = 0
    while i < len(lines):
        if re.match("@entry", lines[i]):
            lines.insert(i, "-----")
            i += 1
        i += 1
    return lines

#Any Kopaeva

def tagLink(lines): #проставляет ссылки для слов(лексем), у которых нет словарных статей, а их словарная статья у другого слова
    for i in range(len(lines)):
        if lines[i].startswith("@entry"):
            lines[i] = re.sub("см. ([А-ЯЁ\-' ])", "@link:\\1", lines[i])
        if lines[i].startswith("@lex"):
            lines[i] = re.sub("см. ([А-ЯЁ\-' ])", "@link:\\1", lines[i])
            if re.search("см. [А-ЯЁ\-' ]", lines[i]):
                pass

    return lines


# уже написанная нами функция по замене слов на теги
def tagMany(lines, d):
    for i in range(len(lines)):
        for key in d:
            a0 = str(d[key])
            a1 = str(d[key])
            b = '\t\t@' + str(key) + ":"
            if lines[i].startswith(str(a0)):
                lines[i] = lines[i].replace(a0 + '.', b)
                lines[i] = lines[i].replace(a0, b)
            lines[i] = lines[i].replace(a1, b)
            if lines[i].startswith('◊'):
                lines[i] = lines[i].replace('◊', '\t@idioms:')

            g1 = 'УПРАВЛЕНИЕ 1.'
            g2 = 'УПРАВЛЕНИЕ 2.'
            if lines[i].startswith(g1) or lines[i].startswith(g2):
                lines[i] = lines[i].replace(g1, '\t\t@government: \n\t\t\t@num: 1')
                lines[i] = lines[i].replace(g2, '\t\t@government: \n\t\t\t@num: 2')

    return lines


#Any Kopaeva

# отмечаем лексемы с помощью регулярных выражений
def tagLexeme(lines):
    lines += [""]
    flag = "NILL"
    buf = "#"
    for i in range(len(lines) - 1):
        if (lines[i].startswith("@entry")):
            flag = "ARTICLE_STARTS"
            if (re.match("@entry:([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'-]+)*[1-9]*, [А-ЯЁ]+)", lines[i])): #несколько слов через пробел,
                buf = re.search("([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'-]+)*[1-9]*), [А-ЯЁ]+", lines[i]).groups()[0]
            else:
                if (re.match("@entry:([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[i])): #слово\n либо слово, <что-то>
                    buf = re.search("@entry:([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[i]).groups()[0]
            if (buf != "#"):
                s = ""
                for j in range(len(buf)):
                    if buf[j] == "Ё":
                        s += "Е"
                    elif buf[j].isalpha():
                        s += buf[j]
                    elif buf[j] == " " or buf[j] == "-":
                        s += buf[j]
                buf = s
                buf = buf.lower()
        elif (lines[i].startswith("I")):
            pass
        elif (flag == "ARTICLE_STARTS"):
            if (re.match(buf + " \d", lines[i + 1])):
                lines[i] = "@summ:" + lines[i]
                flag = "IS_SUMMARY"
            elif re.match("[а-я]+ \d", lines[i + 1]) and (buf[0] == lines[i + 1][0]):
                lines[i] = "@summ:" + lines[i]
                flag = "IS_SUMMARY"
            elif (lines[i].startswith(buf + " 1")):
                lines[i] = "@lex:" + lines[i]
                flag = "IS_LEX"
            else:
                flag = "NILL"
                buf = "#"
        elif (flag == "IS_LEX"):
            if (re.match(buf + " \d", lines[i])):
                lines[i] = "@lex:" + lines[i]
                flag = "IS_LEX"
            elif (lines[i] == "-----"):
                buf = "#"
                flag = "NILL"
            elif lines[i].startswith("ЗНАЧЕНИЕ.") and not(lines[i - 2].startswith("@") and not(lines[i - 1].startswith("@"))) and re.match("[а-я]+ \d", lines[i - 2]):
                if (len(lines[i - 2]) >= 3 and len(buf) >= 3 and lines[i - 2][:3] == buf[:3]):
                    lines[i - 2] = "@lex:" + lines[i - 2] #а это потому, что под одним словом могут написать другие!!! на него похожие!
            else:
                pass
        elif (flag == "IS_SUMMARY"):
            if (lines[i].startswith(buf + " 1.1") or lines[i].startswith(buf + " 1,") or lines[i] == (buf + " 1")):
                lines[i] = "@lex:" + lines[i]
                flag = "IS_LEX"
            else:
                pass
    lines.pop()
    return lines

#Any Kopaeva

# добавляем тег лексемы для однозначных слов
def tagOneDefLex(lines):
    lines += [""]
    flag = "NILL"
    for i in range(len(lines)):
        if (lines[i].startswith("@entry")):
            flag = "ARTICLE_STARTS"
        elif (lines[i].startswith("I")):
            pass
        elif flag == "ARTICLE_STARTS":
            if (lines[i].startswith("@lex") or lines[i].startswith("@summ") or lines[i] == "-----"):
                flag = "NILL"
            else:
                lines[i - 1] = "@entry:@lex:" + lines[i - 1][7:]
                flag = "IS_LEX"
    lines.pop()
    return lines

#Any Kopaeva

# отмечаем примеры (с учетом того, что мы отметили лексемы)
def tagExamples(lines):
    lines = [""] + lines
    lines.append("")
    for i in range(1, len(lines) - 1):
        if (lines[i - 1].startswith("@lex:") or lines[i - 1].startswith("@entry:@lex:")) and (lines[i + 1].startswith("ЗНАЧЕНИЕ.")):
            if lines[i].startswith("\t\t"):
                lines[i] = "\t\t@examples:" + lines[i][2:]
            else:
                lines[i] = "\t\t@examples:" + lines[i]
    lines.pop(0)
    lines.pop()
    return lines

#Any Kopaeva

def tabsLexeme(lines):
    #for i in range(len(lines)):
    i = 0
    while i < len(lines):
        if lines[i].startswith("@lex:"):
            lines[i] = "\t@lex:" + lines[i][5:]
            lines[i + 1] = lines[i + 1]
        if lines[i].startswith("@entry:@lex:"):
            s1 = lines[i][12:]
            if (re.match("@entry:@lex:([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'-]+)*[1-9]*, [А-ЯЁ]+)", lines[i])): #несколько слов через пробел,
                word = re.search("([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'-]+)*[1-9]*), [А-ЯЁ]+", lines[i]).groups()[0]
            else:
                if (re.match("@entry:@lex:([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[i])): #слово\n либо слово, <что-то>
                    word = re.search("@entry:@lex:([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[i]).groups()[0]
            word = word.lower()
            s = ""
            for j in range(len(word)):
               if word[j] != "'":
                    s += word[j]
            word = s
            lines[i] = "@entry:" + s1
            lines.insert(i + 1, "\t@lex:" + word)
            i += 1
        i += 1
    return lines
##мое


# разделение управления на части, необходимые для поиска
def tagGov(lines):
    for i in range(len(lines)):
        if lines[i].startswith('\t•'):
            lines[i] = lines[i-1][:2] + lines[i]
    for i in range(len(lines)):
        if lines[i][0] == 'А' and (len(lines[i]) >= 3 and lines[i][2] == '\t'):
            lines[i] = re.sub('^(А[0123456])', '\t\t\t@actant: \\1', lines[i])
            lines[i] = re.sub('•\s(.+):\s(.+)?', '\n\t\t\t@case: \\1 \n\t\t\t@example: \\2', lines[i])
            lines[i] = re.sub('•\s(.+)', '\n\t\t\t@case: \\1', lines[i])
        elif lines[i][0] == 'А' and (len(lines[i]) >= 3 and lines[i][3] == '+'):
            lines[i] = re.sub('^(А[0123456])', '\t\t\t@actant: \\1', lines[i])
            lines[i] = re.sub('•\s(.+):\s(.+)?', '\n\t\t\t@case: \\1 \n\t\t\t@example: \\2', lines[i])
            lines[i] = re.sub('•\s(.+)', '\n\t\t\t@case: \\1', lines[i])
    return lines



# отмечаем иллюстрации (те, которые с 2 и более авторами)
def tagIllustrations(lines):
    for i in range(len(lines)):
        if '@' not in lines[i]:
            lines[i] = re.sub('^\s?(.+\(.+\)\.\s?)(.+\(.+\)\.\s?)+\s?$', '\t\t@illustrations: \\1\\2', lines[i])
            lines[i] = re.sub('^ (.+\(.+\)\.\s?)(.+\(.+\)\.\s?)+\s?$', '\t\t@illustrations: \\1\\2', lines[i])
    return lines


# разрешаем случаи, где коллокаций много
# идут после тега на новых строчках под номерами
def resolveMultipleCollocations(lines):
    for i in range(len(lines)):
        if lines[i].startswith('\t\t@collocations') and lines[i+1][0] != '\t':
            lines[i+1] = '\t\t\t@col_text: ' + lines[i+1]
        if lines[i].startswith('\t\t\t@col_text:') and lines[i+1][0] != '\t' and lines[i+1][0] != '#':
            lines[i+1] = '\t\t\t@col_text: ' + lines[i+1]
    for i in range(len(lines)):
        if lines[i].startswith('\t\t@collocations') and '@col_text' not in lines[i+1]:
            lines[i] = lines[i][:17] + '\n\t\t\t@col_text: ' + lines[i][17:]
    return lines


# разрешаем случаи, где концструкций много
# идут после тега на новых строчках под номерами
# функция идентична предидущей, меняем только тег
def resolveMultipleConstructions(lines):
    for i in range(len(lines)):
        if lines[i].startswith('\t\t@constructions') and lines[i+1][0] != '\t':
            lines[i+1] = '\t\t\t@text_con: ' + lines[i+1]
        if lines[i].startswith('\t\t\t@text_con') and lines[i+1][0] != '\t':
            lines[i+1] = '\t\t\t@text_con: ' + lines[i+1]
    for i in range(len(lines)):
        if lines[i].startswith('\t\t@constructions') and '@text_con' not in lines[i+1]:
            lines[i] = lines[i][:18] + '\n\t\t\t@text_con: ' + lines[i][18:]
    return lines


# отмечаем комментарии под номерами
def tagNumberedComments(lines):
    for i in range(len(lines)):
        lines[i] = re.sub('^\s?(1\.)', '\t\t@comments: \n\t\t\t@com_text: \\1', lines[i])
        lines[i] = re.sub('^\s?(\d\.)', '\t\t\t@com_text: \\1', lines[i])
    return lines


# отмечаем комментарии по ключевым словам (словарь в начале этого файла)
def resolveComments(lines, arr):
    for i in range(len(lines)):
        for key_word in arr:
            if key_word in lines[i]:
                if lines[i][0] == ' ':
                    lines[i] = '\t\t@comments: \n\t\t\t@com_text: ' + lines[i]
    return lines


# проверяем, что ни в одной строчке не встречается 2 тега
def check(lines):
    for i in range(len(lines)):
        m = re.search('\t\t@[a-z_]+:.+?@', lines[i])
        if m is not None:
            pass
    return 'No'


#Any Kopaeva

def popLines(lines):
    i = 0
    flag = False
    while i < len(lines):
        if lines[i].startswith("I"):
            lines.pop(i)
            i -= 1
        elif not flag:
            if lines[i].startswith("@summ"):
                lines.pop(i)
                flag = True
                i -= 1
        elif flag:
            if re.search("@lex", lines[i]) or re.search("@entry", lines[i]):
                flag = False
            else:
                lines.pop(i)
                i -= 1
        i += 1
    return lines

#Any Kopaeva

def diffLexemes(lines):
    for i in range(len(lines)):
        if not re.search("@", lines[i]) and re.match("[а-я]", lines[i]):
            if (i + 1 < len(lines) and lines[i + 1].startswith("\t\t@definition")):
                lines[i] = "\t@lex:" + lines[i]
            elif (i + 2 < len(lines) and lines[i + 2].startswith("\t\t@definition")):
                lines[i] = "\t@lex:" + lines[i]
                lines[i + 1] = "\t\t@examples:" + lines[i + 1]
        elif re.match("[А-ЯЁ]([а-я'])+ \d", lines[i]): #Всякие лексемы, уважительно начинающиеся с большой буквы
            lines[i] = "\t@lex:" + lines[i]
            lines[i + 1] = "\t\t@examples:" + lines[i + 1]
    return lines

#Функции, отличающей комментарии от иллюстраций, не существует
#поэтому здесь будет функция, ставящая одинокий текст в комментарий
#т.к. одиноких комментариев больше, чем иллюстраций
def hideHoles(lines):
    for i in range(len(lines)):
        if re.match("( )*[\[А-ЯЁа-я]+", lines[i]):
            lines[i] = "\t\t@comments:" + lines[i]
        if re.match("( )*– [А-ЯЁа-я]+", lines[i]):
            lines[i] = "\t\t@comments:" + lines[i]
    return lines

#Any Kopaeva

def unificateTags(lines):
    for i in range(len(lines)):
        if re.search("@", lines[i]):
            lines[i] = re.sub("@entry:(\S)", "@entry: \\1", lines[i])
            lines[i] = re.sub("@lex:(\S)", "@lex: \\1", lines[i])
            lines[i] = re.sub("@examples:(\S)", "@examples: \\1", lines[i])
            lines[i] = re.sub("@comments:(\S)", "@comments: \\1", lines[i])
            lines[i] = re.sub("@link:(\S)", "@link: \\1", lines[i])
            lines[i] = re.sub("@analog:(\S)", "@analog: \\1", lines[i])
            lines[i] = re.sub("@idioms:(\S)", "@idioms: \\1", lines[i])
    return lines

#Any Kopaeva

def many_new_files(lines, fname, number):
    if lines[-1] != '-----':
        lines.append('-----')
    flag = "NILL"
    for i in range(len(lines)):
        if lines[i].startswith("@entry"):
            s = []
            flag = "IS_ENTRY"
            #f = codecs.open(fname + "_" + str(number) + '.txt', 'w', 'utf-8')
            #f.write(lines[i] + "\n")
            s.append(lines[i])
        elif flag == "IS_ENTRY":
            if lines[i] == '-----':
                try:
                    s = make(s, number)
                except:
                    #print(s, "Ai-ai-ai!")
                    s = ""
                f = codecs.open(fname + "_" + str(number) + '.json', 'w', 'utf-8')
                f.write(s)
                f.close()
                flag = "NILL"
                number += 1
                #f.close()
            else:
                #f.write(lines[i] + "\n")
                s.append(lines[i])
    return number

#Any Kopaeva

def make(lines, id):
    file = ""
    file += "{\n"
    file += "\t\"id\": " + str(id) + "," + "\n"
    i = 1
    #
    if len(lines) == 1: #статья-ссылка
        word = re.match("@entry: ([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[0]).groups()[0]
        word = word.lower()
        file += "\t\"word\": " + "\"" + word + "\",\n"
        parts_of = {"СУЩ", "ПРИЛ", "ГЛАГ", "НАРЕЧ", "ПРЕДЛОГ", "СОЮЗ"}
        pos = -1
        for j in parts_of:
            if re.search(j, lines[0]):
                pos = j
        if pos != -1:
            file += "\t\"pos\": \"" + pos + "\"" + ",\n" #а если несколько? "весь"
        if re.search("@link: ([А-ЯЁ1-9'\-­]+)", lines[0]):
            link = re.search("@link: ([А-ЯЁ1-9'\-­]+)", lines[0]).groups()[0]
            file += "\t\"link\": " + "\"" + link + "\",\n"
        file = file[:len(file) - 2] + "\n"
        file += "}"
        return file
    #<word>
    if (re.match("@entry: ([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'-]+)*[1-9]*, [А-ЯЁ]+)", lines[0])): #несколько слов через пробел,
        word = re.search("([А-ЯЁ'\-]+ [А-ЯЁ'\-]+( [А-ЯЁ'-]+)*[1-9]*), [А-ЯЁ]+", lines[0]).groups()[0]
    else:
        if (re.match("@entry: ([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[0])): #слово\n либо слово, <что-то>
            word = re.search("([А-ЯЁ1-9'\-]+)(, [А-ЯЁ]*)*", lines[0]).groups()[0]
    word = word.lower()
    file += "\t\"word\": \"" + word + "\"" + ",\n"
    #</word>
    #<pos>
    parts_of = ["СУЩ", "ПРИЛ", "ГЛАГ", "НАРЕЧ", "ПРЕДЛОГ", "СОЮЗ"]
    pos = -1
    for j in parts_of:
        if re.search(j, lines[0]):
            pos = j
    if pos != -1:
        file += "\t\"pos\": \"" + pos + "\"" + ",\n" #а если несколько? "весь"
    #</pos>
    #<more>
    j = lines[0].find(";")
    if j != -1:
        j += 1
        more = ""
        for j in range(j + 1, len(lines[0])):
            more += lines[0][j]
        file += "\t\"more\": " + "\"" + more + "\"" + ",\n"
    #</more>
    file += "\t\"subentry\": [\n"
    flag = "NILL"
    for i in range(len(lines)):
        #print(lines[i])
        if lines[i].startswith("\t@lex: "): #начало лексемы
            file += "\t\t{\n"
            s = lines[i][7:]
            lex = s
            if lex[-1] == " ":
                lex = lex[:len(lex) - 1]
            file += "\t\t\t\"lexeme\": \"" + lex + "\",\n"
            flag = "IS_LEX"
            if (i < len(lines) - 1 and lines[i + 1].startswith("\t@lex:")):
                file = file[:len(file) - 2]
                file += "\n\t\t},\n"
        elif flag == "IS_LEX":
            if re.match("\t\t@[a-z_]+: ", lines[i]) and ((i + 1 < len(lines) and not(lines[i + 1].startswith("\t\t\t@"))) or (i + 1 == len(lines))): #за ключом есть текст
                key = re.match("\t\t@([a-z_]+): +", lines[i]).groups()[0]
                j = 2 + len(key) + 3
                text = lines[i][j:]
                while text[-1] == " ":
                    text = text[:len(text) - 1]
                file += "\t\t\t\"" + key + "\": " + "\"" + text + "\", \n"
            elif re.match("\t\t@[a-z]+:", lines[i]) or (i + 1 < len(lines) and (lines[i + 1].startswith("\t\t\t@"))): #ключ, за ним пойдет список
                if lines[i + 1].startswith("\t\t\t@"):
                    key = re.match("\t\t@([a-z_]+):+", lines[i]).groups()[0]
                    if lines[i + 1].startswith("\t\t\t@com_text") or lines[i + 1].startswith("\t\t\t@col_text") or lines[i + 1].startswith("\t\t\t@text_con"):
                        file += "\t\t\t\"" + key + "\": \""
                        flag = "IS_LONGSTRING"
                    else:
                        file += "\t\t\t\"" + key + "\": [\n"
                        file += "\t\t\t\t{\n"
                        flag = "IS_LIST"
            elif lines[i].startswith("\t@"):
                if file[-2] == ",":
                    file = file[:len(file) - 2] + "\n"
                if file[-2] == " " and file[-3] == ",":
                    file = file[:len(file) - 3] + "\n"
                key = re.match("\t@([a-z]+): ", lines[i]).groups()[0]
                file += "\t\t}\n" #последний элемент, запятая не требуется
                file += "\t],\n"
                j = 1 + len(key) + 3
                file += "\t\"" + key + "\": " + "\"" + lines[i][j:] + "\",\n"
                flag = "IS_ENDING"
            if (i + 1 < len(lines) and lines[i + 1].startswith("\t@lex")):  #конец лексемы
                if file[-2] == ",":
                    file = file[:len(file) - 2] + "\n"
                if file[-2] == " " and file[-3] == ",":
                    file = file[:len(file) - 3] + "\n"
                file += "\t\t},\n"
        elif flag == "IS_LIST":
            key = re.match("\t\t\t@([a-z_]+): ", lines[i]).groups()[0]
            j = 4 + len(key) + 2
            text = lines[i][j:]
            if text[-1] == "\t":  #
                text = text[:len(text) - 1]
            while text[-1] == " ":
                    text = text[:len(text) - 1]  #
            file += "\t\t\t\t\t\"" + key + "\": " + "\"" + text + "\",\n"
            if lines[i + 1].startswith("\t\t\t@actant:"):
                file = file[:len(file) - 2] + "\n"
                file += "\t\t\t\t},\n"
                file += "\t\t\t\t{\n"
            if lines[i + 1].startswith("\t\t@") or lines[i + 1].startswith("\t@"): #конец списка
                if file[-2] == "{":
                    file = file[:len(file) - 2] + "\n"
                if file[-2] == ",":
                    file = file[:len(file) - 2] + "\n"
                file += "\t\t\t\t}\n"
                file += "\t\t\t],\n"
                flag = "IS_LEX"
        elif flag == "IS_ENDING":
            key = re.match("\t@([a-z]+): ", lines[i]).groups()[0]
            j = 1 + len(key) + 3
            file += "\t\"" + key + "\": " + "\"" + lines[i][j:] + "\",\n"
            flag = "IS_ENDING"
            if i == len(lines) - 1:
                file = file[:len(file) - 2] + "\n"
                file += "}"
        elif flag == "IS_LONGSTRING":
            key = re.match("(\t\t\t@[a-z_]+: )", lines[i]).groups()[0]
            text = lines[i][len(key):]
            file += text + "/nl/"
            if (i + 1 < len(lines) and (lines[i + 1].startswith("\t\t@") or lines[i + 1].startswith("\t@"))):
                file = file[:len(file) - 4]
                file += "\",\n"
                flag = "IS_LEX"
    file = file[:len(file) - 2] + "\n"
    file += "}"
    return file

#Any Kopaeva

def deleteN(lines):
    i = 0
    while i < len(lines):
        while len(lines[i]) > 1 and lines[i][0] == ' ':
            lines[i] = lines[i][1:]
        j = lines[i].find("\n")
        if j != -1:
            s = lines[i][j + 1:]
            lines[i] = lines[i][:j]
            lines.insert(i + 1, s)
        j = lines[i].find("\t\t@", 4, len(lines[i]))
        if j != -1 and j != 0 and j != 1:
            s = lines[i][j + 1:]
            while(len(s) > 1 and s[0] == ' '):
                s = s[1:]
            if lines[i].startswith("\t\t") and s.startswith("\t@"):
                s = "\t" + s
            if lines[i].startswith("@\t\t\t") and s.startswith("t\t@"):
                s = "\t" + s
            lines[i] = lines[i][:j]
            lines.insert(i + 1, s)
        i += 1
    return lines

def workActants(lines):
    flag = "NILL"
    i = 0
    while i < len(lines):
        if lines[i].startswith("\t\t\t@actant"):
            flag = "IS_ACTANT"
            key = lines[i][12:]
        elif flag == "IS_ACTANT" and (lines[i].find("@") == -1) and lines[i] != '-----':
            if lines[i].find(":") == -1:
                case = re.search("([а-яА-ЯЁ -]+)", lines[i]).groups()[0]
                s = "\t\t\t" + "@actant: " + key
                lines.insert(i, s)
                i += 1
                lines[i] = "\t\t\t" + "@case: " + case
            else:
                j = lines[i].find(":")
                case = lines[i][:j]
                while case[0] == " " or case[0] == "\t":
                    case = case[1:]
                example = lines[i][j + 2:]
                s = "\t\t\t" + "@actant: " + key
                lines.insert(i, s)
                i += 1
                s = "\t\t\t" + "@case: " + case
                lines.insert(i, s)
                i += 1
                lines[i] = "\t\t\t" + "@example: " + example
        elif not lines[i].startswith("\t\t\t@"):
            flag = "NILL"
        i += 1

    return lines



def main():
    index = 0
    for i in ["A", "B", "V1", "V2", "G"]:
        fname = i
        f0 = codecs.open(fname + '.txt', 'r', 'utf-8')
        f1 = codecs.open(fname + '_formatted.txt', 'w', 'utf-8')

        s = f0.read()
        whole_letter = s.split('\n')
        whole_letter = cleaner(whole_letter)

        whole_letter = tagAuthor(whole_letter) # <Any Kopaeva>
        whole_letter = tagEntry(whole_letter)
        whole_letter = cleanend(whole_letter)
        whole_letter = resolveEntries(whole_letter)
        whole_letter = tagLexeme(whole_letter)
        whole_letter = tagOneDefLex(whole_letter)
        whole_letter = tagLink(whole_letter)
        whole_letter = tagExamples(whole_letter)
        whole_letter = tabsLexeme(whole_letter) # </Any Kopaeva>

        whole_letter = tagMany(whole_letter, d_tags)
        whole_letter = tagGov(whole_letter)
        whole_letter = tagIllustrations(whole_letter)
        whole_letter = resolveMultipleCollocations(whole_letter)
        whole_letter = resolveMultipleConstructions(whole_letter)
        whole_letter = tagNumberedComments(whole_letter)
        whole_letter = resolveComments(whole_letter, comment_key_words)

        whole_letter = popLines(whole_letter) # <Any Kopaeva>
        whole_letter = diffLexemes(whole_letter)
        whole_letter = hideHoles(whole_letter)
        whole_letter = unificateTags(whole_letter)
        whole_letter = deleteN(whole_letter)
        whole_letter = workActants(whole_letter)
        index = many_new_files(whole_letter, fname, index) # </Any Kopaeva>
        print(check(whole_letter))


        s = '\n'.join([line for line in whole_letter])
        # s = re.sub('\n\n', '\n', s)
        f1.write(s)

        f0.close()
        f1.close()

main()