# -*- coding: utf-8 -*-
import codecs
import os
from elasticsearch import Elasticsearch
es = Elasticsearch()

arr = []
iss = []
for root, dirs, files in os.walk('v'):
    i = 0
    for f in files:
        if f in arr:
            print('double ' + f)
        else:
            json = codecs.open(root + os.sep + f, 'r', 'utf-8')
            body = json.read()
            try:
                es.index(index="dict", doc_type="entry", body=body)
                print(str(i))
            except:
                print('Check ' + f + '!')
                iss.append(f)
            arr.append(f)
            i+=1
            json.close()
j = 0
for i in iss:
    print(i)
    j+=1
print(j)
'''

res = es.search(index="actived", body={"query": {"match": {"pos": u"ГЛАГ"}}})
print("Got %d Hits:" % res['hits']['total'])

for hit in res['hits']['hits']:
    print hit["_source"]["word"]

'''
